<?php
include("header.php");
include("fonction.php");
   
    // definition des informations
$servername = 'localhost';
$username = 'root';
$dbname = 'chaussurechic';
$password = '';    
$conn = new mysqli($servername,$username,$password,$dbname);
if($conn-> connect_error){die("connexion failed".$conn-> connect_error);}
$req = mysqli_query($conn,"SELECT produits.nom,photo.photo1,produits.idproduit,produits.prix FROM produits,photo where produits.idproduit=photo.idproduit AND produits.genre='F';");

$contenu = "";
$contenu .= "<! DOCTYPE html>
<html>
    <head>
    <style>
    
    .buy-btn:hover{
      color:#FFFFFF;
      background-color: #f15fa3;
      transition: all ease 0.3s;
  
  }
  .buy-btn{
      width:160px;
      height: 40px;
      display: flex;
      justify-content: center;
      align-items: center;
      background-color: #FFFFFF;
      color: #252525;
      font-weight: 700;
      letter-spacing: 1px;
      font-family: calibri;
      border-radius: 20px;
      box-shadow: 2px 2px 30px rgba(0,0,0,0.2);
    margin-left:10px;
  
  
  
  }
  .overlay{
    position: absolute;
    left:50%;
    top: 50%;
    transform: translate(-50%,-50%);
    width:100%;
    height:100%;
    background-color: rgba(92,95,236,0.6);
    display:flex;
    justify-content: center;
    align-items: center;

}
.overlay{
    visibility: hidden;
}
#slide-img:hover .overlay{
  visibility: visible;
  animation: fade 0.5s;
}
@keyframes fade{
  0%{
      opacity: 0;
  }
  50%{
      opacity: 1;
  }
}
.detail-box{
  width:100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 10px 20px;
  box-sizing: border-box;
  font-family: calibri;

}
.type{
  display: flex;
  flex-direction: column;
}
.type a{
  color: #222222;
  margin: 5px 0px;
  font-weight: 700;
  letter-spacing: 0.5px;
  padding-right: 8px;

}
.price{
  color: #333333;
  font-weight: 600;
  font-size: 1.1rem;
  font-family: poppins;
  letter-spacing: 0.5px;

}
.type span{
  color:rgba(26,26,26,0.5);

}


    </style>
    <title>Homme</title>
    <link rel=\"shortcut icon\" href=\"logo.png\">
        
        <meta name=\"viewport\" content=\"width=width-device, initial-scale=1\">
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">
        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>
        <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Sofia\">
        <link href=\"album.css\" rel=\"stylesheet\">
   
    
 
   
		<!-- pour mettre les icones -->
		<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.0/css/all.css\" integrity=\"sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ\" crossorigin=\"anonymous\">
    </head>
    <body>
        <div style=\"height:60px;margin-top:40px;margin-left:50px\">
        <pre><a href=\"accueilFrancais.php\" style=\"color:black\"><i class=\"fas fa-long-arrow-alt-left\"></i> <b>RETOUR</b></a>  Accueil / Femmes / Chaussures</pre>
        

        </div>
        <div style=\"height:70px;margin-left:50px\">
        <h1 style=\"font-size:40px\"><b>CHAUSSURES FEMMES</b></h1>
        </div><div class=\"row\">
";
while ($row=mysqli_fetch_row($req)){
   
  $contenu .=
  "
  <div class=\"col-md-3\">
  <div class=\"card mb-4 shadow-sm\" id=\"slide-img\">
   <img src=\"$row[1]\" alt=\"image introuvable\" width=356 height=350/></text>
   <div class=\"overlay\">
   <a href=\"ficheproduit.php?idproduit=$row[2]\" class=\"buy-btn\" style=\"text-decoration:none\">voir plus</a>
</div>
<div class=\"detail-box\">
          
          
<div class=\"type\">
   <a href=\"#\">$row[0]</a>
   <span>In store</span>

</div>
<a href=\"#\" class=\"price\">$$row[3]</a>
</div>
  </div>
</div>

   ";

    
   }
   $contenu .= "</div>
   </body>
</html>";
echo $contenu;
?>



<?php
include("footerA.php")
?>

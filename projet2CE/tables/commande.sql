-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 28 déc. 2020 à 17:40
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chaussurechic`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

CREATE TABLE `commande` (
  `idcommande` int(20) NOT NULL,
  `date` varchar(25) NOT NULL,
  `etat` varchar(30) NOT NULL,
  `prix` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`idcommande`, `date`, `etat`, `prix`) VALUES
(1, '12.27.20', 'en cours', 0),
(2, '12.27.20', 'en cours', 0),
(3, '12.27.20', 'en cours', 207),
(4, '12.27.20', 'en cours', 621),
(5, '12.27.20', 'en cours', 207),
(6, '12.27.20', 'en cours', 0),
(7, '12.27.20', 'en cours', 207),
(8, '12.27.20', 'en cours', 0),
(9, '12.27.20', 'en cours', 207),
(10, '12.27.20', 'en cours', 0),
(11, '12.27.20', 'en cours', 0),
(12, '12.27.20', 'en cours', 207),
(13, '12.27.20', 'en cours', 621),
(14, '12.27.20', 'en cours', 0),
(15, '12.28.20', 'en cours', 288),
(16, '12.28.20', 'en cours', 0),
(17, '12.28.20', 'en cours', 828),
(18, '12.28.20', 'en cours', 0),
(19, '12.28.20', 'en cours', 0),
(20, '12.28.20', 'en cours', 207),
(21, '12.28.20', 'en cours', 25875),
(22, '12.28.20', 'en cours', 3933),
(23, '12.28.20', 'en cours', 3669),
(24, '12.28.20', 'en cours', 598),
(25, '12.28.20', 'en cours', 3726),
(26, '12.28.20', 'en cours', 966),
(27, '12.28.20', 'en cours', 8280),
(28, '12.28.20', 'en cours', 414),
(29, '12.28.20', 'en cours', 2277),
(30, '12.28.20', 'en cours', 414),
(31, '12.28.20', 'en cours', 207),
(32, '12.28.20', 'en cours', 207),
(33, '12.28.20', 'en cours', 207),
(34, '12.28.20', 'en cours', 0),
(35, '12.28.20', 'en cours', 207),
(36, '12.28.20', 'en cours', 414),
(37, '12.28.20', 'en cours', 966),
(38, '12.28.20', 'en cours', 897),
(39, '12.28.20', 'en cours', 207),
(40, '12.28.20', 'en cours', 414),
(41, '12.28.20', 'en cours', 0),
(42, '12.28.20', 'en cours', 207),
(43, '12.28.20', 'en cours', 207),
(44, '12.28.20', 'en cours', 207),
(45, '12.28.20', 'en cours', 207),
(46, '12.28.20', 'en cours', 0),
(47, '12.28.20', 'en cours', 207),
(48, '12.28.20', 'en cours', 207),
(49, '12.28.20', 'en cours', 0),
(50, '12.28.20', 'en cours', 0),
(51, '12.28.20', 'en cours', 0),
(52, '12.28.20', 'en cours', 207),
(53, '12.28.20', 'en cours', 207),
(54, '12.28.20', 'en cours', 207),
(55, '12.28.20', 'en cours', 0),
(56, '12.28.20', 'en cours', 207),
(57, '12.28.20', 'en cours', 207),
(58, '12.28.20', 'en cours', 207),
(59, '12.28.20', 'en cours', 207),
(60, '12.28.20', 'en cours', 207),
(61, '12.28.20', 'en cours', 621),
(62, '12.28.20', 'en cours', 207),
(63, '12.28.20', 'en cours', 0),
(64, '12.28.20', 'en cours', 0),
(65, '12.28.20', 'en cours', 207),
(66, '12.28.20', 'en cours', 0),
(67, '12.28.20', 'en cours', 207),
(68, '12.28.20', 'en cours', 207),
(69, '12.28.20', 'en cours', 207),
(70, '12.28.20', 'en cours', 621),
(71, '12.28.20', 'en cours', 621),
(72, '12.28.20', 'en cours', 207),
(73, '12.28.20', 'en cours', 414),
(74, '12.28.20', 'en cours', 0),
(75, '12.28.20', 'en cours', 207),
(76, '12.28.20', 'en cours', 1242),
(77, '12.28.20', 'en cours', 552),
(78, '12.28.20', 'en cours', 207);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`idcommande`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `commande`
--
ALTER TABLE `commande`
  MODIFY `idcommande` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

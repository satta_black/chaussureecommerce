-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 28 déc. 2020 à 17:39
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chaussurechic`
--

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `idproduit` int(11) NOT NULL,
  `nom` varchar(300) NOT NULL,
  `types` varchar(300) NOT NULL,
  `couleur` varchar(300) NOT NULL,
  `genre` varchar(300) NOT NULL,
  `type` varchar(300) NOT NULL,
  `prix` int(11) NOT NULL,
  `propiete` varchar(300) NOT NULL,
  `description` text NOT NULL,
  `caracteristique` text NOT NULL,
  `stock` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`idproduit`, `nom`, `types`, `couleur`, `genre`, `type`, `prix`, `propiete`, `description`, `caracteristique`, `stock`) VALUES
(1, 'CHAUSSURE PHARRELL WILLIAMS SUPERSTAR PRIMEKNIT', 'Dark Green / Dark Green / Sky Tint', 'verte', 'M', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(2, 'CHAUSSURE PHARRELL WILLIAMS SUPERSTAR PRIMEKNIT', 'Dark Green / Dark Green / Sky Tint\r\n', 'jaune', 'M', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.\r\n\r\n', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(3, 'CHAUSSURE PHARRELL WILLIAMS SUPERSTAR PRIMEKNIT', 'Dark Green / Dark Green / Sky Tint\r\n', 'gris', 'M', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.\r\n\r\n', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(4, 'CHAUSSURE PHARRELL WILLIAMS SUPERSTAR PRIMEKNIT', 'Dark Green / Dark Green / Sky Tint\r\n', 'kaki', 'M', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.\r\n\r\n', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(5, 'CHAUSSURE SUPERSTAR ADV X ', 'Core Black / Scarlet / Gold Metallic\r\n', 'noire', 'M', 'ete', 140, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Cette version de la Superstar ADV est le fruit de la collaboration entre adidas Skateboarding et Trasher magazine. La chaussure présente une tige en cuir souple et un shell-toe classique.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(6, 'CHAUSSURE ULTRABOOST 5.0 DNA', 'Football Blue / Football Blue / Royal Blue', 'bleu', 'M', 'ete', 250, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'L’esprit dans le cosmos, le cœur sur la planète. Cette chaussure de running Ultraboost adidas célèbre notre partenariat avec la NASA avec des badges Artemis sur la languette. Porte-la pour de longues promenades en ville ou pour ramasser les déchets sur la plage. Sa tige adidas Primeknit épouse les contours du pied, tandis que sa semelle extérieure Boost offre un confort ultime.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(7, 'CHAUSSURE SUPERSTAR RUN-DMC', 'Cloud White / Core Black / Hi-Res Red', 'blanc', 'M', 'ete', 160, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Certains disent que l\'âge d\'or du hip-hop a débuté en 1983 à Hollins, dans le Queens. C\'est la ville natale de Rev. Run, DMC et Jam Master Jay. Ils se sont emparés de leur époque, ont révolutionné les mondes de la musique et de la mode. Et ils portaient la chaussure à shell-toe comme personne. Célébrant les 50 ans de la sneaker adidas Superstar, cette chaussure adidas Superstar est inspirée de ces chansons qui nous restent en tête. Elle est ornée de détails spéciaux comme des bouts de lacets avec logos adidas et d\'une semelle de propreté et étiquette de languette Run-DMC.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(8, 'CHAUSSURE SUPERSTAR RUN-DMC', 'Cloud White / Core Black / Hi-Res Red', 'noir\r\n', 'M', 'ete', 160, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Certains disent que l\'âge d\'or du hip-hop a débuté en 1983 à Hollins, dans le Queens. C\'est la ville natale de Rev. Run, DMC et Jam Master Jay. Ils se sont emparés de leur époque, ont révolutionné les mondes de la musique et de la mode. Et ils portaient la chaussure à shell-toe comme personne. Célébrant les 50 ans de la sneaker adidas Superstar, cette chaussure adidas Superstar est inspirée de ces chansons qui nous restent en tête. Elle est ornée de détails spéciaux comme des bouts de lacets avec logos adidas et d\'une semelle de propreté et étiquette de languette Run-DMC.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(50, 'CHAUSSURE PHARRELL WILLIAMS ', 'Dark Green / Dark Green / Sky Tint', 'blanc', 'F', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(51, 'CHAUSSURE PHARRELL GIRL', 'Dark Green / Dark Green / Sky Tint', 'noir', 'F', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(52, 'CHAUSSURE PHA GIRL ', 'Dark Green / Dark Green / Sky Tint', 'noir', 'F', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(53, 'CHAUSSURE PHA GIRL ', 'Dark Green / Dark Green / Sky Tint', 'orange', 'F', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Au cours des 50 dernières années, la chaussure Superstar adidas \r\nest devenue synonyme de game changer au niveau culturel. \r\nAujourd\'hui elle rencontre Pharrell. Multi-talents, curieux, \r\navant-gardistes... Pharrell Williams apporte sa touche personnelle\r\n à la célèbre sneaker à shell-toe. Il n\'y a rien de plus logique.', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(100, 'CHAUSSURE SUPERSTAR', 'Cloud White / Core Black / Cloud White', 'blanc', 'E', 'ete', 300, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'La version miniature d\'un grand classique. Tout comme le modèle pour adultes, cette chaussure bébés affiche les lignes épurées et le style casual de la célèbre adidas Superstar. La tige en cuir souple est doublée en textile pour un maximum de confort. Facile à enfiler et à retirer, elle est équipée de bandes auto-agrippantes.\r\n\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(101, 'CHAUSSURE SUPERSTAR RED', 'Cloud White / Core Black / Cloud White', 'rouge', 'E', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'La version miniature d\'un grand classique. Tout comme le modèle pour adultes, cette chaussure bébés affiche les lignes épurées et le style casual de la célèbre adidas Superstar. La tige en cuir souple est doublée en textile pour un maximum de confort. Facile à enfiler et à retirer, elle est équipée de bandes auto-agrippantes.\r\n\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(102, 'CHAUSSURE SUPERSTAR BLANC BLACK', 'Cloud White / Core Black / Cloud White', 'Blanc', 'E', 'ete', 300, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'La version miniature d\'un grand classique. Tout comme le modèle pour adultes, cette chaussure bébés affiche les lignes épurées et le style casual de la célèbre adidas Superstar. La tige en cuir souple est doublée en textile pour un maximum de confort. Facile à enfiler et à retirer, elle est équipée de bandes auto-agrippantes.\r\n\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(103, 'CHAUSSURE SUPERSTAR BLANC ', 'Cloud White / Core Black / Cloud White', 'Blanc', 'E', 'ete', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'La version miniature d\'un grand classique. Tout comme le modèle pour adultes, cette chaussure bébés affiche les lignes épurées et le style casual de la célèbre adidas Superstar. La tige en cuir souple est doublée en textile pour un maximum de confort. Facile à enfiler et à retirer, elle est équipée de bandes auto-agrippantes.\r\n\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(200, 'Botte Fairbanks™ Omni-Heat pour femme', 'Cloud White / Core Black / Cloud White', 'noir', 'F', 'hiver', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Chaleur confortable et imperméable dans cette botte d\'inspiration athlétique pour des aventures hivernales quotidiennes. La botte Fairbanks Omni-Heat pour homme garde vos pieds au chaud grâce à l\'association d\'une isolation légère et d\'une doublure thermo-réfléchissante. Son chausson imperméable et respirant, avec une construction aux coutures scellées, vous protège contre la neige, la gadoue, la boue et la pluie.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(201, 'Botte Fairbanks™ Omni-Heat ', 'Cloud White / Core Black / Cloud White', 'kaki', 'M', 'hiver', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Chaleur confortable et imperméable dans cette botte d\'inspiration athlétique pour des aventures hivernales quotidiennes. La botte Fairbanks Omni-Heat pour homme garde vos pieds au chaud grâce à l\'association d\'une isolation légère et d\'une doublure thermo-réfléchissante. Son chausson imperméable et respirant, avec une construction aux coutures scellées, vous protège contre la neige, la gadoue, la boue et la pluie.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(202, 'Botte Fairbanks™ Homme', 'Cloud White / Core Black / Cloud White', 'kaki', 'M', 'hiver', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Chaleur confortable et imperméable dans cette botte d\'inspiration athlétique pour des aventures hivernales quotidiennes. La botte Fairbanks Omni-Heat pour homme garde vos pieds au chaud grâce à l\'association d\'une isolation légère et d\'une doublure thermo-réfléchissante. Son chausson imperméable et respirant, avec une construction aux coutures scellées, vous protège contre la neige, la gadoue, la boue et la pluie.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite'),
(203, 'Botte Fairbanks™ Omni-Heat enfants', 'Cloud White / Core Black / Cloud White', 'blanc', 'E', 'hiver', 180, 'PRODUIT EXCLU DE TOUTES LES PROMOTIONS, COUPONS ET BONS.', 'Chaleur confortable et imperméable dans cette botte d\'inspiration athlétique pour des aventures hivernales quotidiennes. La botte Fairbanks Omni-Heat pour homme garde vos pieds au chaud grâce à l\'association d\'une isolation légère et d\'une doublure thermo-réfléchissante. Son chausson imperméable et respirant, avec une construction aux coutures scellées, vous protège contre la neige, la gadoue, la boue et la pluie.\r\n', 'Tige textile adidas Primeknit.\r\nSneaker ultra confortable.\r\nSemelle extérieure en caoutchouc.\r\nCouleur du produit : Dark Green / Dark Green / Sky Tint\r\nCode du produit : S42928', 'illimite');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`idproduit`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

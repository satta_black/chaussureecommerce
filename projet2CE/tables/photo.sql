-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 28 déc. 2020 à 17:39
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chaussurechic`
--

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE `photo` (
  `idproduit` int(50) NOT NULL,
  `photo1` varchar(50) NOT NULL,
  `photo2` varchar(50) NOT NULL,
  `photo3` varchar(50) NOT NULL,
  `photo4` varchar(50) NOT NULL,
  `photo5` varchar(50) NOT NULL,
  `photo6` varchar(50) NOT NULL,
  `photo7` varchar(50) NOT NULL,
  `photo8` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `photo`
--

INSERT INTO `photo` (`idproduit`, `photo1`, `photo2`, `photo3`, `photo4`, `photo5`, `photo6`, `photo7`, `photo8`) VALUES
(101, 'enfants/cccccc.jpg', 'enfants/ccccc.jpg', 'enfants/cccc.jpg', 'enfants/ccc.jpg', 'enfants/cc.jpg', 'enfants/c.jpg', '', ''),
(100, 'enfants/dddd.jpg', 'enfants/ddd.jpg', 'enfants/dd.jpg', 'enfants/d.jpg', '', '', '', ''),
(102, 'enfants/bbbbbb.jpg', 'enfants/bbbbb.jpg', 'enfants/bbbb.jpg', 'enfants/bbb.jpg', 'enfants/bb.jpg', 'enfants/b.jpg', '', ''),
(103, 'enfants/aaaaaa.jpg', 'enfants/aaaaa.jpg', 'enfants/aaaa.jpg', 'enfants/aaa.jpg', 'enfants/aa.jpg', 'enfants/a.jpg', '', ''),
(50, 'femmes/zzzzzz.jpg', 'femmes/zzzzzzzzz.jpg', 'femmes/zzzzz.jpg', 'femmes/zzzz.jpg', 'femmes/zzz.jpg', 'femmes/zz.jpg', 'femmes/z.jpg', ''),
(51, 'femmes/cccccc.jpg', 'femmes/ccccc.jpg', 'femmes/cccc.jpg', 'femmes/cc.jpg', 'femmes/c.jpg', '', '', ''),
(52, 'femmes/bbbbb.jpg', 'femmes/bbbb.jpg', 'femmes/bbb.jpg', 'femmes/bb.jpg', 'femmes/b.jpg', '', '', ''),
(53, 'femmes/aaaa.jpg', 'femmes/aaa.jpg', 'femmes/aa.jpg', 'femmes/a.jpg', '', '', '', ''),
(1, 'hommes/aaaa.jpg', 'hommes/aaa.jpg', 'hommes/aa.jpg', 'hommes/a.jpg', '', '', '', ''),
(2, 'hommes/bbbbb.jpg', 'hommes/bbbb.jpg', 'hommes/bbb.jpg', 'hommes/bb.jpg', 'hommes/b.jpg', '', '', ''),
(3, 'hommes/ccccc.jpg', 'hommes/cccc.jpg', 'hommes/ccc.jpg', 'hommes/cc.jpg', 'hommes/c.jpg', '', '', ''),
(4, 'hommes/ddddd.jpg', 'hommes/dddd.jpg', 'hommes/ddd.jpg', 'hommes/dd.jpg', 'hommes/d.jpg', '', '', ''),
(5, 'hommes/eeeee.jpg', 'hommes/eeee.jpg', 'hommes/eee.jpg', 'hommes/ee.jpg', 'hommes/e.jpg', '', '', ''),
(6, 'hommes/fffff.jpg', 'hommes/ffff.jpg', 'hommes/fff.jpg', 'hommes/ff.jpg', 'hommes/f.jpg', '', '', ''),
(7, 'hommes/ggggg.jpg', 'hommes/gggg.jpg', 'hommes/ggg.jpg', 'hommes/gg.jpg', 'hommes/g.jpg', '', '', ''),
(8, 'hommes/hhhh.jpg', 'hommes/hhh.jpg', 'hommes/hh.jpg', 'hommes/h.jpg', '', '', '', ''),
(200, 'hiver/a.jpg', 'hiver/aa.jpg', 'hiver/aaa.jpg', 'hiver/aaaa.jpg', '', '', '', ''),
(201, 'hiver/b.jpg', 'hiver/bb.jpg', 'hiver/bbb.jpg', 'hiver/bbbb.jpg', '', '', '', ''),
(202, 'hiver/c.jpg', 'hiver/cc.jpg', 'hiver/ccc.jpg', 'hiver/cccc.jpg', '', '', '', ''),
(203, 'hiver/d.jpg', 'hiver/dd.jpg', 'hiver/ddd.jpg', 'hiver/dddd.jpg', '', '', '', '');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `photo`
--
ALTER TABLE `photo`
  ADD KEY `idproduit` (`idproduit`);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`idproduit`) REFERENCES `produits` (`idproduit`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

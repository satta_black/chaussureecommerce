-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 28 déc. 2020 à 17:39
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.3.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chaussurechic`
--

-- --------------------------------------------------------

--
-- Structure de la table `taille`
--

CREATE TABLE `taille` (
  `idproduit` varchar(20) NOT NULL,
  `taille1` varchar(20) NOT NULL,
  `taille2` varchar(20) NOT NULL,
  `taille3` varchar(20) NOT NULL,
  `taille4` varchar(20) NOT NULL,
  `taille5` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `taille`
--

INSERT INTO `taille` (`idproduit`, `taille1`, `taille2`, `taille3`, `taille4`, `taille5`) VALUES
('1', '39cm', '40cm', '41cm', '42cm', '43cm'),
('2', '39cm', '40cm', '41cm', '42cm', '43cm'),
('3', '39cm', '40cm', '41cm', '42cm', '43cm'),
('4', '39cm', '40cm', '41cm', '42cm', '43cm'),
('5', '39cm', '40cm', '41cm', '42cm', '43cm'),
('6', '39cm', '40cm', '41cm', '42cm', '43cm'),
('7', '39cm', '40cm', '41cm', '42cm', '43cm'),
('7', '39cm', '40cm', '41cm', '42cm', '43cm'),
('8', '39cm', '40cm', '41cm', '42cm', '43cm'),
('50', '30cm', '31cm', '32cm', '33cm', '34cm'),
('51', '30cm', '31cm', '32cm', '33cm', '34cm'),
('52', '30cm', '31cm', '32cm', '33cm', '34cm'),
('53', '30cm', '31cm', '32cm', '33cm', '34cm'),
('100', '20cm', '21cm', '22cm', '23cm', '24cm'),
('101', '20cm', '21cm', '22cm', '23cm', '24cm'),
('102', '20cm', '21cm', '22cm', '23cm', '24cm'),
('103', '20cm', '21cm', '22cm', '23cm', '24cm'),
('200', '30cm', '31cm', '32cm', '33cm', '34cm'),
('201', '39cm', '40cm', '41cm', '42cm', '43cm'),
('202', '39cm', '40cm', '41cm', '42cm', '43cm'),
('203', '20cm', '21cm', '22cm', '23cm', '24cm'),
('1', '39cm', '40cm', '41cm', '42cm', '43cm'),
('2', '39cm', '40cm', '41cm', '42cm', '43cm'),
('3', '39cm', '40cm', '41cm', '42cm', '43cm'),
('4', '39cm', '40cm', '41cm', '42cm', '43cm'),
('5', '39cm', '40cm', '41cm', '42cm', '43cm'),
('6', '39cm', '40cm', '41cm', '42cm', '43cm'),
('7', '39cm', '40cm', '41cm', '42cm', '43cm'),
('7', '39cm', '40cm', '41cm', '42cm', '43cm'),
('8', '39cm', '40cm', '41cm', '42cm', '43cm'),
('50', '30cm', '31cm', '32cm', '33cm', '34cm'),
('51', '30cm', '31cm', '32cm', '33cm', '34cm'),
('52', '30cm', '31cm', '32cm', '33cm', '34cm'),
('53', '30cm', '31cm', '32cm', '33cm', '34cm'),
('100', '20cm', '21cm', '22cm', '23cm', '24cm'),
('101', '20cm', '21cm', '22cm', '23cm', '24cm'),
('102', '20cm', '21cm', '22cm', '23cm', '24cm'),
('103', '20cm', '21cm', '22cm', '23cm', '24cm'),
('200', '30cm', '31cm', '32cm', '33cm', '34cm'),
('201', '39cm', '40cm', '41cm', '42cm', '43cm'),
('202', '39cm', '40cm', '41cm', '42cm', '43cm'),
('203', '20cm', '21cm', '22cm', '23cm', '24cm');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
